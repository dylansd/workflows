package com.dksd.flows.kafka;

public class ContainersVersions {
    public static final String KAFKA = "confluentinc/cp-kafka:5.5.0";
    public static final String SCHEMA_REGISTRY = "confluentinc/cp-schema-registry:5.5.0";
    public static final String REDIS = "redis:5.0.8-alpine3.11";
    public static final String BUILDER_DB_POSTGRES = "postgres:11";
    public static final String TOXIC_PROXY = "shopify/toxiproxy:2.1.0";
}
