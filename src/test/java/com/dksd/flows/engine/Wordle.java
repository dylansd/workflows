package com.dksd.flows.engine;

import com.dksd.flows.common.helper.JsonHelper;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Wordle {
    static Map<Character, Integer> freqLetters = new HashMap<>();

    public static void main(String[] args) throws IOException {
        JsonNode words = JsonHelper.parseJson(new File("words_dictionary.json"));
        Iterator<String> it = words.fieldNames();
        Map<Character, Integer> letterToPos = new HashMap<>();

        int i = 0;
        //Pos -0-5 means in the word but in wrong place.
        //Pos -99 not in the word
        //Pos == 99 means wildcard
        // 0-4 means we know where in the word  exactly
        //oreas  e none
        letterToPos.put('l', 1);
        letterToPos.put('a', -99);
        letterToPos.put('t', -99);
        letterToPos.put('e', 4);
        letterToPos.put('r', -99);
        letterToPos.put('s', -99);
        letterToPos.put('i', -99);
        letterToPos.put('d', -99);
        //letterToPos.put('u', -99);
        //letterToPos.put('b', -99);

        while (it.hasNext()) {
            String word = it.next();
            boolean validWord = checkWord(word, letterToPos);
            if (!validWord) {
                continue;
            }
            System.out.println(word);
            i++;
        }
        System.out.println("Number of matching words: " + i);
    }

    private static boolean checkWord(String word, Map<Character, Integer> letterToPos) {
        if (word.length() != 5) {
            return false;
        }
        for (int i = 0; i < word.length(); i++) {
            char ch = word.charAt(i);
            freqLetters.computeIfAbsent(ch, k -> 0);
            freqLetters.put(ch, freqLetters.get(ch) + 1);
        }
        for (Map.Entry<Character, Integer> entry : letterToPos.entrySet()) {
            //skip words without the char
            if (entry.getValue() == -99 && word.contains(String.valueOf(entry.getKey()))) {
                return false;
            }
            //skip words with wildcard in the wrong place
            if (entry.getValue() < 0 && entry.getValue() != -99
                    && word.charAt(Math.abs(entry.getValue())) == entry.getKey()) {
                return false;
            }
            //skip words without the wildcard
            if (entry.getValue() == 99 && !word.contains(String.valueOf(entry.getKey()))) {
                return false;
            }
            //match exact characters
            if (entry.getValue() >= 0
                    && entry.getValue() <= 4
                    && !(word.charAt(entry.getValue()) == entry.getKey())) {
                return false;
            }
        }
        return true;
    }

}
