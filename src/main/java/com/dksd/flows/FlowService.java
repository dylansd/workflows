package com.dksd.flows;

import com.dksd.flows.common.client.FlowClient;
import com.dksd.flows.common.exceptions.FlowNotFoundException;
import com.dksd.flows.common.model.Flow;
import com.dksd.flows.common.model.FlowId;
import com.dksd.flows.common.model.FlowInfo;
import com.dksd.flows.common.model.User;
import com.dksd.flows.common.schema.Annotation;
import com.dksd.flows.common.schema.FlowMsgData;

import java.io.IOException;
import java.util.Map;

public class FlowService {

  protected final FlowClient flowClient;
  private final Map<FlowId, Flow> flowCache;

  private final FlowInfo flowInfo;

  public FlowService(FlowClient flowClient, Map<FlowId, Flow> flowCache, FlowInfo flowInfo) {
    this.flowClient = flowClient;
    this.flowCache = flowCache;
    this.flowInfo = flowInfo;
  }

  public Flow getFlow(User user, FlowMsgData fm) throws FlowNotFoundException {
    return getFlow(user, flowInfo.getFlowId(fm.getId()));
  }

  public Flow getFlow(User user, Annotation anno) throws FlowNotFoundException {
    return getFlow(user, flowInfo.getFlowId(anno.getAnnoId()));
  }

  public Flow getFlow(User user, FlowId flowId) throws FlowNotFoundException {
    if (flowCache.containsKey(flowId)) {
      return flowCache.get(flowId);
    }
    Flow flow = null;
    try {
      flow = flowClient.getFlow(
        flowId.getFlowId(),
        user.getUserId().getUserId()).execute().body();
    } catch (IOException e) {
      //TODO need a retry here
      e.printStackTrace();
      throw new FlowNotFoundException("Could not get flow: " + flowId);
    }
    if (flow == null) {
      throw new FlowNotFoundException(flowId.toString());
    }
    flowCache.put(flowId, flow);
    return flow;
  }

}
