package com.dksd.flows;

import com.dksd.flows.common.client.FlowClient;
import com.dksd.flows.common.client.UserClient;
import com.dksd.flows.common.helper.ExpiringMap;
import com.dksd.flows.common.kafka.DataKey;
import com.dksd.flows.common.model.*;
import com.dksd.flows.config.FlowEngineConfig;
import com.dksd.flows.kafka.CommonFlowMsg;
import com.dksd.flows.kafka.streams.AnnotationKStreams;
import com.dksd.flows.kafka.streams.FlowMsgDataToAnnoKStreams;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class FlowStreamsApplication {

    private static final FlowEngineConfig flowEngineConfig = new FlowEngineConfig();
    private static final CountDownLatch latch = new CountDownLatch(3);

    private static void runKafkaStreams(KafkaStreams streams) {
        streams.setStateListener((newState, oldState) -> {
            if (oldState == KafkaStreams.State.RUNNING && newState != KafkaStreams.State.RUNNING) {
                latch.countDown();
            }
        });
        streams.start();
    }

    public static void main(String[] args) throws Exception {
        if (args.length < 1) {
            throw new IllegalArgumentException("This program takes one argument: the path to a configuration file.");
        }

        Properties props = new Properties();
        try (InputStream inputStream = Files.newInputStream(Paths.get(args[0]))) {
            props.load(inputStream);
        }

        FlowEngineConfig.addAllProperties(props);
        String webServerAddress = props.getProperty("web.server.eai.url");
        String annoInputTopic = props.getProperty("annotation.input.topic.name");
        String annoOutputTopic = props.getProperty("annotation.output.topic.name");
        String flowMsgDataInputTopic = props.getProperty("flowmsgdata.input.topic.name");
        String flowMsgDataOutputTopic = props.getProperty("flowmsgdata.output.topic.name");
        String annoStreamAppId = props.getProperty("annotation.stream.id");
        String flowMsgStreamAppId = props.getProperty("flowmsg.stream.id");
        String apiServiceToken = props.getProperty("eai.flows.service.token");

        UserClient userClient = flowEngineConfig.getUserClient(webServerAddress, apiServiceToken);
        FlowClient flowClient = flowEngineConfig.getFlowClient(webServerAddress, apiServiceToken);
        ExpiringMap<UserId, User> userCache = new ExpiringMap<>(7, TimeUnit.DAYS);
        ExpiringMap<FlowId, Flow> flowCache = new ExpiringMap<>(7, TimeUnit.DAYS);

        Producer<String, String> dlProducer = getDeadLettersProducer(props);

        FlowInfo flowInfo = flowEngineConfig.getFlowInfo();
        flowEngineConfig.getTrackerConsumer(flowInfo);

        UserService userService = new UserService(userClient, userCache);
        FlowService flowService = new FlowService(flowClient, flowCache, flowInfo);

        //TODO this is what you want:
        //https://kafka.apache.org/10/documentation/streams/developer-guide/config-streams.html#default-deserialization-exception-handler
        StreamsUncaughtExceptionHandler excHandler = new StreamsUncaughtExceptionHandler() {
            @Override
            public StreamThreadExceptionResponse handle(Throwable exception) {
                return StreamThreadExceptionResponse.REPLACE_THREAD;
            }
        };
        CommonFlowMsg commonFlowMsg = new CommonFlowMsg();
        runKafkaStreams(new AnnotationKStreams(annoStreamAppId, annoInputTopic, annoOutputTopic, userService, flowService, props, dlProducer, flowInfo, excHandler)
                .createKStream());
        runKafkaStreams(new FlowMsgDataToAnnoKStreams(
                commonFlowMsg,
                flowMsgStreamAppId,
                flowMsgDataInputTopic,
                flowMsgDataOutputTopic,
                userService,
                flowService,
                props,
                dlProducer,
                flowInfo,
                excHandler)
                .createKStream());

        try {
            latch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static Producer<String, String> getDeadLettersProducer(Properties props) {
        props.put("client.id", "eai_dl_producer");
        return new KafkaProducer<>(props);
    }

    public static Producer<DataKey, String> getDataProducer(Properties props) {
        props.put("client.id", "eai_data_producer");
        props.put("key.serializer", "com.dksd.flows.common.kafka.DataKeySerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        return new KafkaProducer<>(props);
    }


}