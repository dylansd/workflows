package com.dksd.flows.factory;

import com.dksd.flows.common.function.core.AbstractFlowMemFunction;
import com.dksd.flows.config.FlowEngineConfig;
import com.dksd.flows.function.HttpClientFunction;

import java.util.HashMap;
import java.util.Map;

/**
 * Return function that could be executed or returns empty annotations for later async results.
 */
public class FunctionFactory {
    //private String baseUrl;
    private Map<String, String> headers;

    public FunctionFactory() {
        init();
    }

    private void init() {
        //baseUrl = FlowEngineConfig.getProperty("huggingface.base.url"); //"https://api-inference.huggingface.co/models/distilbert-base-uncased";
        String api_token = FlowEngineConfig.getProperty("hugginface.api.token");
        headers = new HashMap<>();//getHeaders("headers", payload);
        headers.put("Authorization", "Bearer " + api_token);
    }

    public AbstractFlowMemFunction loadFunction(String url, String method) {
        return new HttpClientFunction(
                url,
                method,
                headers);
        /*if (isSummarizeText(functionName)) {
            return huggingfaceModel("google/pegasus-xsum", "post");
        }
        if (isTextGeneration(functionName)) {
            return huggingfaceModel("gpt2", "post");
        }
        if (isTextConversation(functionName)) {
            return huggingfaceModel("facebook/blenderbot-400M-distill", "post");
        }
        if (isQuestionGeneration(functionName)) {
            return huggingfaceModel("mrm8488/t5-base-finetuned-question-generation-ap", "post");
        }
        if (isTextClassification(functionName)) {
            return huggingfaceModel("distilbert-base-uncased-finetuned-sst-2-english", "post");
        }
        if (isTextToImage(functionName)) {
            return huggingfaceModel("osanseviero/dalle-mini-fork", "post");
        }*/

        /*if (isTextResponse(functionName)) {
            return new AbstractFlowMemFunction() {
                @Override
                public List<Annotation> handleMessage(FlowMsgData flowMsg) {
                    JsonNode node = null;
                    try {
                        node = getParsedPayload(flowMsg);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                    int options_to_create = 3;
                    if (node.get("options_to_create") != null) {
                        node.get("options_to_create").asInt(3);
                    }
                    List<Annotation> annos = new ArrayList<>(options_to_create);
                    List<String> hardcodedResponses = new ArrayList<>();
                    hardcodedResponses.add("Not your buddy pal.");
                    hardcodedResponses.add("Will look at it.");
                    hardcodedResponses.add("Hi there, definitely thanks for reaching out and will look at it.");
                    System.out.println("Text response kicking in: " + flowMsg);
                    for (int i = 0; i < options_to_create; i++) {
                        annos.addAll(Annotation.ofKV(flowMsg, "text_response", hardcodedResponses.get(i)));
                    }
                    System.out.println("Annotations created: " + annos);
                    return annos;
                }
            };
        }*/
        //Slack functions to send and not use web server instead receive events from slack....
        //https://api.slack.com/start/building/bolt-java

        /*Integer hashCode = functionStr.hashCode();
        if (functionCache.containsKey(hashCode)) {
            return functionCache.get(hashCode);
        }
        logger.info("Verifying: " + functionStr + " with sig: " + new String(signature));
        FlowCreator.getInstance().verify(functionStr.getBytes(StandardCharsets.UTF_8), signature);
        Object o = null;
        try {
            byte[] data = Base64.getDecoder().decode(functionStr);
            ObjectInputStream ois = new ObjectInputStream(
                    new ByteArrayInputStream(data));
            o = ois.readObject();
            ois.close();
        }
        catch (Exception e) {
            logger.severe(e.getMessage());
        }
        functionCache.put(hashCode, (AbstractFlowMemFunction) o);
        return functionCache.get(hashCode);*/
    }

    /*private boolean isTextClassification(FuncMeta functionName) {
        return FuncMeta.TEXT_CLASSIFICATION.equals(functionName);
    }

    private boolean isQuestionGeneration(FuncMeta functionName) {
        return FuncMeta.TEXT_QUESTION_GEN.equals(functionName);
    }

    private boolean isTextConversation(FuncMeta functionName) {
        return FuncMeta.TEXT_CONVERSATION.equals(functionName);
    }

    private boolean isTextGeneration(FuncMeta functionName) {
        return FuncMeta.TEXT_GENERATION.equals(functionName);
    }

    private boolean isSummarizeText(FuncMeta functionName) {
        return FuncMeta.TEXT_SUMMARIZATION.equals(functionName);
    }

    private boolean isTextToImage(FuncMeta functionName) {
        return FuncMeta.TEXT_TO_IMAGE.equals(functionName);
    }*/

    /*private boolean isTextResponse(FuncMeta functionName) {
        return FuncMeta.TEXT_CLASSIFICATION.equals(functionName);
    }*/

}
