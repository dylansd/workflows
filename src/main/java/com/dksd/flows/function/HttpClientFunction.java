package com.dksd.flows.function;

import com.dksd.flows.common.function.core.AbstractFlowMemFunction;
import com.dksd.flows.common.schema.Annotation;
import com.dksd.flows.common.schema.FlowMsgData;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.net.http.HttpResponse.PushPromiseHandler;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.logging.Logger;

public class HttpClientFunction extends AbstractFlowMemFunction {

    @JsonIgnore
    private static final Logger logger = Logger.getLogger("HttpClientFunction");
    static HttpClient client = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_2)
            .build();
    private Map<String, String> headers;
    private String url;
    private String method;

    //, String modelId, String apiToken
    public HttpClientFunction(String url,
                              String method,
                              Map<String, String> headers) {
        this.url = url;
        this.method = method;
        this.headers = headers;
    }

    //For async background processing
    //private final ExecutorService executorService = Executors.newCachedThreadPool();
    //private final KafkaProducer<UUID, Annotation> annotationProducer;
    //private KafkaProducer<UUID, String> deadLetterProducer;

    /*
    Need to define the json for requests:
    url:
    method: get.post.put
    headers:
    sync/async:


    public static void main(String[] args) throws Exception {
        //httpGetRequest();
        //httpPostRequest();
        asynchronousGetRequest();
        asynchronousMultipleRequests();
        pushRequest();
    }*/

    public static HttpResponse<String> httpGetRequest(
            String url, Map<String, String> headers) throws URISyntaxException, IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest.Builder builder = HttpRequest.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .uri(URI.create(url));
        for (Map.Entry<String, String> entry : headers.entrySet()) {
            //builder.headers("Accept-Enconding", "gzip, deflate");
            builder.setHeader(entry.getKey(), entry.getValue());
        }
        HttpRequest request = builder.build();
        return client.send(request, BodyHandlers.ofString());
        /*String responseBody = response.body();
        int responseStatusCode = response.statusCode();

        System.out.println("httpGetRequest: " + responseBody);
        System.out.println("httpGetRequest status code: " + responseStatusCode);*/
    }

    public static HttpResponse<String> httpPostRequest(String url,
                                                       Map<String, String> headers,
                                                       String body) throws URISyntaxException, IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder(
                        new URI(url))
                .version(HttpClient.Version.HTTP_2)
                .setHeader("Authorization", headers.get("Authorization"))
                .POST(BodyPublishers.ofString(body))
                .build();
        HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
        /*if (response.statusCode() != 200) {
            //we have an error.
        }*/
        String responseBody = response.body();
        logger.info("httpPostResponse " + response.statusCode() + " : " + responseBody);
        return response;
    }

    public static CompletableFuture<Void> asynchronousGetRequest(String url) throws URISyntaxException {
        HttpClient client = HttpClient.newHttpClient();
        URI httpURI = new URI(url);
        HttpRequest request = HttpRequest.newBuilder(httpURI)
                .version(HttpClient.Version.HTTP_2)
                //.POST()
                .build();
        return client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                .thenAccept(resp -> {
                    logger.info("Got pushed response " + resp.uri());
                    logger.info("Response statuscode: " + resp.statusCode());
                    logger.info("Response body: " + resp.body());
                });
    }

    /*public static void asynchronousMultipleRequests(String url) throws URISyntaxException {
        HttpClient client = HttpClient.newHttpClient();
        List<URI> uris = Arrays.asList(
                new URI("http://jsonplaceholder.typicode.com/posts/1"),
                new URI("http://jsonplaceholder.typicode.com/posts/2"));
        List<HttpRequest> requests = uris.stream()
                .map(HttpRequest::newBuilder)
                .map(reqBuilder -> reqBuilder.build())
                .collect(Collectors.toList());
        CompletableFuture.allOf(requests.stream()
                        .map(request -> client.sendAsync(request, BodyHandlers.ofString()))
                        .toArray(CompletableFuture<?>[]::new))
                .thenAccept(System.out::println)
                .join();
    }*/

    public static void pushRequest() throws URISyntaxException, InterruptedException {
        logger.info("Running HTTP/2 Server Push example...");

        HttpClient httpClient = HttpClient.newBuilder()
                .version(Version.HTTP_2)
                .build();

        HttpRequest pageRequest = HttpRequest.newBuilder()
                .uri(URI.create("https://http2.golang.org/serverpush"))
                .build();

        // Interface HttpResponse.PushPromiseHandler<T>
        // void applyPushPromise​(HttpRequest initiatingRequest, HttpRequest pushPromiseRequest, Function<HttpResponse.BodyHandler<T>,​CompletableFuture<HttpResponse<T>>> acceptor)
        httpClient.sendAsync(pageRequest, BodyHandlers.ofString(), pushPromiseHandler())
                .thenAccept(pageResponse -> {
                    logger.info("Page response status code: " + pageResponse.statusCode());
                    System.out.println("Page response headers: " + pageResponse.headers());
                    String responseBody = pageResponse.body();
                    logger.info(responseBody);
                }).join();

        Thread.sleep(1000); // waiting for full response
    }

    private static PushPromiseHandler<String> pushPromiseHandler() {
        return (HttpRequest initiatingRequest,
                HttpRequest pushPromiseRequest,
                Function<HttpResponse.BodyHandler<String>,
                        CompletableFuture<HttpResponse<String>>> acceptor) -> {
            acceptor.apply(BodyHandlers.ofString())
                    .thenAccept(resp -> {
                        logger.info(" Pushed response: " + resp.uri() + ", headers: " + resp.headers());
                    });
            logger.info("Promise request: " + pushPromiseRequest.uri());
            logger.info("Promise request: " + pushPromiseRequest.headers());
        };
    }

    //TODO might want a publisher too to be able to send annotations
    //for async calls. for now keep en sync.
    @Override
    public List<Annotation> handleMessage(FlowMsgData flowMsg) throws Exception {
        JsonNode payload = getParsedPayload(flowMsg);
        long st = System.currentTimeMillis();
        HttpResponse<String> response = null;
        switch (method.toLowerCase()) {
            case "post":
                String body = getBody(payload); //"The goal of life is [MASK]."
                response = httpPostRequest(url, headers, body);
                break;
            case "get":
                response = httpGetRequest(url, headers);
                break;
            default:
                break;
        }
        long ed = System.currentTimeMillis();
        logger.info("time taken: " + (ed - st));
        if (response != null && (response.statusCode() == 200 ||
                response.statusCode() == 201)) {
            return Annotation.error(flowMsg, response.body(), null);
        }
        return Annotation.ofString(flowMsg, response.body());
    }

    private String getBody(JsonNode payload) {
        return payload.get("body").asText();
    }

    private String getHttpMethod(JsonNode payload) {
        return payload.get("http_method").asText();
    }

    private Map<String, String> getHeaders(String field, JsonNode payload) {
        return getMapFromJson(payload.get(field));
    }

    private String getUrl(JsonNode payload) {
        return payload.get("url").asText();
    }
}