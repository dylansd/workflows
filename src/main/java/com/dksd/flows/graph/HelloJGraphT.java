package com.dksd.flows.graph;

import com.dksd.flows.common.model.Tracker;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.nio.Attribute;
import org.jgrapht.nio.DefaultAttribute;
import org.jgrapht.nio.dot.DOTExporter;
import org.jgrapht.traverse.DepthFirstIterator;

import java.io.StringWriter;
import java.io.Writer;
import java.rmi.server.ExportException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

/**
 * A simple introduction to using JGraphT.
 *
 * @author Barak Naveh
 */
public final class HelloJGraphT {
    private HelloJGraphT() {
    } // ensure non-instantiability.

   /* public static void main(String[] args) throws ExportException {
        Graph<String, DefaultEdge> stringGraph = createStringGraph();

        // note undirected edges are printed as: {<v1>,<v2>}
        System.out.println("-- toString output");
        System.out.println(stringGraph.toString());
        System.out.println();

        // create a graph based on Tracker objects
        Graph<Tracker, DefaultEdge> hrefGraph = createHrefGraph();

        // find the vertex corresponding to www.jgrapht.org
        Tracker start = hrefGraph
                .vertexSet().stream().filter(t -> t.getId().toString().contains("a")).findAny()
                .get();

        // perform a graph traversal starting from that vertex

        System.out.println("-- renderHrefGraph output");
        renderHrefGraph(hrefGraph);
        System.out.println();
    }
*/
    /**
     * Creates a toy directed graph based on Tracker objects that represents link structure.
     *
     * @return a graph based on Tracker objects.
     */
    private static Graph<Tracker, DefaultEdge> createHrefGraph() {

        Graph<Tracker, DefaultEdge> g = new DefaultDirectedGraph<>(DefaultEdge.class);

        Tracker google = new Tracker(UUID.randomUUID());
        Tracker wikipedia = new Tracker(UUID.randomUUID());
        Tracker jgrapht = new Tracker(UUID.randomUUID());

        // add the vertices
        g.addVertex(google);
        g.addVertex(wikipedia);
        g.addVertex(jgrapht);

        // add edges to create linking structure
        g.addEdge(jgrapht, wikipedia);
        g.addEdge(google, jgrapht);
        g.addEdge(google, wikipedia);
        g.addEdge(wikipedia, google);

        return g;
    }

    /**
     * Traverse a graph in depth-first order and print the vertices.
     *
     * @param hrefGraph a graph based on Tracker objects
     * @param start     the vertex where the traversal should start
     */
    private static void traverseHrefGraph(Graph<Tracker, DefaultEdge> hrefGraph, Tracker start) {
        Iterator<Tracker> iterator = new DepthFirstIterator<>(hrefGraph, start);
        while (iterator.hasNext()) {
            Tracker Tracker = iterator.next();
            System.out.println(Tracker);
        }
    }

    /**
     * Render a graph in DOT format.
     *
     * @param hrefGraph a graph based on Tracker objects
     */
    private static void renderHrefGraph(Graph<Tracker, DefaultEdge> hrefGraph) {
        DOTExporter<Tracker, DefaultEdge> exporter =
                new DOTExporter<>();
        exporter.setVertexAttributeProvider((v) -> {
            Map<String, Attribute> map = new LinkedHashMap<>();
            map.put("label", DefaultAttribute.createAttribute(v.toString()));
            return map;
        });
        Writer writer = new StringWriter();
        exporter.exportGraph(hrefGraph, writer);
        System.out.println(writer.toString());
    }

    /**
     * Create a toy graph based on String objects.
     *
     * @return a graph based on String objects.
     */
    private static Graph<String, DefaultEdge> createStringGraph() {
        Graph<String, DefaultEdge> g = new SimpleGraph<>(DefaultEdge.class);

        String v1 = "v1";
        String v2 = "v2";
        String v3 = "v3";
        String v4 = "v4";

        // add the vertices
        g.addVertex(v1);
        g.addVertex(v2);
        g.addVertex(v3);
        g.addVertex(v4);

        // add edges to create a circuit
        g.addEdge(v1, v2);
        g.addEdge(v2, v3);
        g.addEdge(v3, v4);
        g.addEdge(v4, v1);

        return g;
    }
}
