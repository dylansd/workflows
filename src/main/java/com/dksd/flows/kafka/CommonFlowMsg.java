package com.dksd.flows.kafka;

import com.dksd.flows.common.client.FlowClient;
import com.dksd.flows.common.client.UserClient;
import com.dksd.flows.common.exceptions.FlowNotFoundException;
import com.dksd.flows.common.exceptions.UserNotFoundException;
import com.dksd.flows.common.helper.FilterRulesResult;
import com.dksd.flows.common.helper.JsonHelper;
import com.dksd.flows.common.helper.RuleResult;
import com.dksd.flows.common.model.*;
import com.dksd.flows.common.schema.Annotation;
import com.dksd.flows.common.schema.FlowMsgData;
import com.dksd.flows.factory.FunctionFactory;
import com.dksd.flows.kafka.streams.AbstractKStreams;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static com.dksd.flows.common.model.CheckPoint.*;
import static com.dksd.flows.common.model.CheckPoint.FLOWMSG_FILTER_UNMATCHED_EXEC;

public class CommonFlowMsg {

  protected final java.util.logging.Logger logger = Logger.getLogger(getClass().getName());
  private FunctionFactory ff = new FunctionFactory();

  public List<Annotation> identifyStoneGetAnnosFromFunction(
    Flow flow,
    FlowMsgData msg,
    FlowInfo flowInfo,
    Producer<String, String> dlProducer) throws UserNotFoundException,
    FlowNotFoundException, JsonProcessingException {
    Objects.requireNonNull(msg);
    List<Annotation> annos = new ArrayList<>();

    logger.fine("Msg is not finished and still processing: " + msg);
    //TODO maybe query in db?
    for (Tracker tracker : flowInfo.get(msg.getId())) {
      if (CheckPoint.ANNO_ROUTING_MATCHED_EXEC.equals(tracker.getCheckPoint())) {
        Stone srcStone = flow.getStoneById(tracker.getSrcStone());
        Stone destStone = flow.getStoneById(tracker.getDestStone());
        annos.addAll(getAnnotationFromDestFunction(flow, srcStone, destStone, msg, flowInfo, dlProducer));
      } else if (CheckPoint.FLOWMSG_IN_CONTROLLER.equals(tracker.getCheckPoint())) {
        annos.addAll(applyStartingFilterRules(flow, msg, flowInfo, dlProducer));
      } else {
        throw new IllegalStateException("FlowMsg Could not match tracker: " + tracker);
      }
    }
    return annos;
  }

  private List<Annotation> getAnnotationFromDestFunction(Flow flow,
                                                         Stone srcStone,
                                                         Stone destStone,
                                                         FlowMsgData msg,
                                                         FlowInfo flowInfo,
                                                         Producer<String, String> dlProducer) {
    List<Annotation> annos = new ArrayList<>();
    try {
      //TODO make this parallel for speed.
      annos.addAll(ff.loadFunction(destStone.getUrl(), destStone.getMethod()).handleMessage(msg));
      for (Annotation anno : annos) {
        commonTracker(MsgType.ANNOTATION,
          FUNC_CREATE_ANNO_OUT,
          anno.getAnnoId(),
          anno.getAnnoData().length(),
          flow,
          srcStone.getId(),
          destStone.getId(),
          null,
          msg.getId(),
          flowInfo);
      }
    } catch (Exception ep) {
      sendToDeadLetter(flow, srcStone, destStone, msg, ep, flowInfo, dlProducer);
    }
    return annos;
  }

  private void sendToDeadLetter(Flow flow,
                                Stone srcStone,
                                Stone destStone,
                                FlowMsgData msg,
                                Exception ep,
                                FlowInfo flowInfo,
                                Producer<String, String> dlProducer) {
    Tracker et = new Tracker();
    et.setId(UUID.randomUUID());
    et.setUserId(flow.getUserId());
    et.setCheckPoint(FUNC_CREATE_ANNO_OUT_ERROR);
    et.setSrcStone(srcStone.getId());
    et.setDestStone(destStone.getId());
    et.setFlowId(flow.getFlowId());
    et.setMsgType(MsgType.ERROR);
    et.setParentId(msg.getId());
    et.setException(ep);
    flowInfo.send(et.getId(), et);
    dlProducer.send(new ProducerRecord<>(msg.getId().toString(), msg.toString()));
  }

  private List<Annotation> applyStartingFilterRules(Flow flow, FlowMsgData msg,
                                                    FlowInfo flowInfo,
                                                    Producer<String, String> dlProducer) throws JsonProcessingException {
    for (Stone stone : flow.getStones().values()) {
      FilterRulesResult fr = stone.applyFilterRules(JsonHelper.getParsedPayload(msg));
      if (fr.getResult()) {
        for (UUID destStoneId : getStoneIds(flow, fr.getDestStoneNames())) {
          commonTracker(MsgType.MSGDATA, FLOWMSG_FILTER_MATCHED_EXEC, msg.getId(),
            msg.getData().length(), flow, stone.getId(), destStoneId, fr, null, flowInfo);
          return getAnnotationFromDestFunction(flow, stone,
            flow.getStoneById(destStoneId), msg, flowInfo, dlProducer);
        }
      } else {
        commonTracker(MsgType.MSGDATA, FLOWMSG_FILTER_UNMATCHED_EXEC, msg.getId(),
          msg.getData().length(), flow, stone.getId(), null, fr, null, flowInfo);
      }

    }
    return Collections.emptyList();
  }

  protected static List<UUID> getStoneIds(Flow flow, Set<String> destStoneNames) {
    return destStoneNames.stream()
      .map(str -> flow.getStoneByName(str).getId())
      .collect(Collectors.toUnmodifiableList());
  }

  protected void commonTracker(MsgType msgType,
                               CheckPoint checkPoint,
                               UUID id,
                               int payloadSize,
                               Flow flow,
                               UUID srcStone,
                               UUID destStone,
                               RuleResult fr,
                               UUID parentId,
                               FlowInfo flowInfo) {
    Tracker tracker = new Tracker(id);
    tracker.setMsgType(msgType);
    tracker.setCheckPoint(checkPoint);
    tracker.setSrcStone(srcStone);
    tracker.setDestStone(destStone);
    if (fr != null) {
      tracker.setFilterRoutingRuleIds(AbstractKStreams.prepareFormat(fr));
    }
    tracker.setUserId(flow.getUserId());
    tracker.setPayloadSizeBytes(payloadSize);
    tracker.setFlowId(flow.getFlowId());
    tracker.setParentId(parentId);
    flowInfo.send(id, tracker);
  }

}
