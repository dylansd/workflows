package com.dksd.flows.kafka.streams;

import com.dksd.flows.FlowService;
import com.dksd.flows.UserService;
import com.dksd.flows.common.client.FlowClient;
import com.dksd.flows.common.client.UserClient;
import com.dksd.flows.common.helper.JsonHelper;
import com.dksd.flows.common.helper.RoutingRulesResult;
import com.dksd.flows.common.helper.RuleResult;
import com.dksd.flows.common.kafka.AnnotationSerdes;
import com.dksd.flows.common.kafka.MsgDataSerdes;
import com.dksd.flows.common.model.*;
import com.dksd.flows.common.schema.Annotation;
import com.dksd.flows.common.schema.FlowMsgData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;

import java.util.*;

import static com.dksd.flows.common.model.CheckPoint.ANNO_ROUTING_MATCHED_EXEC;
import static com.dksd.flows.common.model.CheckPoint.ANNO_ROUTING_UNMATCHED_EXEC;

/**
 * This is my first attempt at Kafka steams to replace existing func.
 */
public class AnnotationKStreams extends AbstractKStreams {
    public final String INPUT_TOPIC; // = TopicDefs.ANNO_TOPIC;
    public final String OUTPUT_TOPIC; // = TopicDefs.FLOWMSGDATA_TOPIC;
    private final Producer<String, String> dlProducer;
    private final String appId;

    public AnnotationKStreams(String appId,
                              String inputTopic,
                              String outputTopic,
                              UserService userService,
                              FlowService flowService,
                              Properties props,
                              Producer<String, String> dlProducer,
                              FlowInfo flowInfo,
                              StreamsUncaughtExceptionHandler exceptionHandler) {
        super(userService, flowService, props, flowInfo, exceptionHandler);
        this.appId = appId;
        this.INPUT_TOPIC = inputTopic;
        this.OUTPUT_TOPIC = outputTopic;
        this.dlProducer = dlProducer;
    }

    @Override
    protected String getApplicationId() {
        return appId;
    }

    @Override
    protected String getClientId() {
        return UUID.randomUUID().toString();
    }

    protected StreamsBuilder buildTopology() {
        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, Annotation> inputStream = builder.stream(INPUT_TOPIC, Consumed.with(Serdes.String(), AnnotationSerdes.instance()));
        KStream<String, FlowMsgData> outputStream = inputStream.flatMap(this::createNextFlowMsgsFromAnno);
        outputStream.to(OUTPUT_TOPIC, Produced.with(Serdes.String(), MsgDataSerdes.instance()));
        return builder;
    }

    public List<KeyValue<String, FlowMsgData>> createNextFlowMsgsFromAnno(String key, Annotation annotation) {
        List<KeyValue<String, FlowMsgData>> msgDataKvs = new ArrayList<>();
        try {
            User user = getUserService().getUser(flowInfo.getUserId(annotation.getAnnoId()));
            Flow flow = getFlowService().getFlow(user, annotation);
            Set<Tracker> trackers = flowInfo.get(annotation.getAnnoId());
            for (Tracker tracker : trackers) {
                if (CheckPoint.FUNC_CREATE_ANNO_OUT.equals(tracker.getCheckPoint())) {
                    for (FlowMsgData flowMsgData : matchAndCreateNextMsgs(flow, flow.getStoneById(tracker.getSrcStone()), annotation)) {
                        msgDataKvs.add(new KeyValue<>(flowMsgData.getId().toString(), flowMsgData));
                    }
                } else {
                    throw new IllegalStateException("Anno stream: Could not match tracker: " + tracker);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            dlProducer.send(createDeadRecord(getClientId(), key, JsonHelper.toJsonNoEx(annotation), e.getMessage()));
        }
        return msgDataKvs;
    }

    public List<FlowMsgData> matchAndCreateNextMsgs(Flow flow,
                                                    Stone srcStone,
                                                    Annotation annotation) throws JsonProcessingException {
        List<FlowMsgData> results = new ArrayList<>();
        if (annotation.getAnnoData() == null) {
            return results;
        }
        JsonNode data = JsonHelper.parseJson(annotation.getAnnoData());
        RoutingRulesResult fr = srcStone.applyRoutingRules(data);
        applyPostRuleLogic(flow, srcStone, annotation, results, fr);
        return results;
    }

    private void applyPostRuleLogic(Flow flow,
                                    Stone srcStone,
                                    Annotation annotation,
                                    List<FlowMsgData> results,
                                    RoutingRulesResult fr) {
        if (fr.getResult()) {
            results.addAll(createMsgFromMatchedRules(
                    flow,
                    srcStone.getId(),
                    fr,
                    annotation));
        } else {//No routing rules were matched
            commonTracker(MsgType.ANNOTATION,
                    ANNO_ROUTING_UNMATCHED_EXEC,
                    annotation.getAnnoId(),
                    annotation.getAnnoData().length(),
                    flow, srcStone.getId(), null, fr, null);
        }
    }

    private List<FlowMsgData> createMsgFromMatchedRules(
            Flow flow,
            UUID srcStoneId,
            RuleResult fr,
            Annotation annotation) {
        List<FlowMsgData> fms = new ArrayList<>();
        for (UUID destStoneId : getStoneIds(flow, fr.getDestStoneNames())) {
            FlowMsgData fmd = createNextFlowMsg(annotation.getAnnoData());
            fms.add(fmd);
            commonTracker(MsgType.MSGDATA,
                    ANNO_ROUTING_MATCHED_EXEC,
                    fmd.getId(),
                    fmd.getData().length(),
                    flow,
                    srcStoneId,
                    destStoneId,
                    fr,
                    annotation.getAnnoId());
        }
        return fms;
    }

    private FlowMsgData createNextFlowMsg(String data) {
        FlowMsgData msgData = new FlowMsgData();
        msgData.setData(data);
        return msgData;
    }
}
