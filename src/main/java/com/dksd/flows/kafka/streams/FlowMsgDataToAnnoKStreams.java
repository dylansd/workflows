package com.dksd.flows.kafka.streams;

import com.dksd.flows.FlowService;
import com.dksd.flows.UserService;
import com.dksd.flows.common.client.FlowClient;
import com.dksd.flows.common.client.UserClient;
import com.dksd.flows.common.helper.JsonHelper;
import com.dksd.flows.common.kafka.AnnotationSerdes;
import com.dksd.flows.common.kafka.MsgDataSerdes;
import com.dksd.flows.common.model.*;
import com.dksd.flows.common.schema.Annotation;
import com.dksd.flows.common.schema.FlowMsgData;
import com.dksd.flows.kafka.CommonFlowMsg;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;

import java.util.*;

/**
 * This is my first attempt at Kafka steams to replace existing func.
 * Beefy but much more compact and removed one whole stream
 */
public class FlowMsgDataToAnnoKStreams extends AbstractKStreams {
  public final String INPUT_TOPIC;
  public final String OUTPUT_TOPIC;
  private Producer<String, String> dlProducer;

  private String appId;
  private CommonFlowMsg commonFlowMsg;

  public FlowMsgDataToAnnoKStreams(
    CommonFlowMsg commonFlowMsg,
    String appId,
    String inputTopic,
    String outputTopic,
    UserService userService,
    FlowService flowService,
    Properties props,
    Producer<String, String> dlProducer,
    FlowInfo flowInfo,
    StreamsUncaughtExceptionHandler exceptionHandler) {
    super(userService, flowService, props, flowInfo, exceptionHandler);
    this.appId = appId;
    this.INPUT_TOPIC = inputTopic;
    this.OUTPUT_TOPIC = outputTopic;
    this.dlProducer = dlProducer;
    this.commonFlowMsg = commonFlowMsg;
  }

  public List<KeyValue<String, Annotation>> mapFlowMsgToAnnotation(String key, FlowMsgData fm) {
    List<KeyValue<String, Annotation>> msgDataKvs = new ArrayList<>(20);
    try {
      User user = getUserService().getUser(flowInfo.getUserId(fm.getId()));
      Flow flow = getFlowService().getFlow(user, fm);
      for (Annotation annotation : commonFlowMsg.identifyStoneGetAnnosFromFunction(
        flow, fm, flowInfo, dlProducer)) {
        msgDataKvs.add(new KeyValue<>(annotation.getAnnoId().toString(), annotation));
      }
    } catch (Exception e) {
      e.printStackTrace();
      dlProducer.send(createDeadRecord(getClientId(), key, JsonHelper.toJsonNoEx(fm), e.getMessage()));
    }
    return msgDataKvs;
  }


  @Override
  protected String getApplicationId() {
    return appId;
  }

  @Override
  protected String getClientId() {
    return UUID.randomUUID().toString();
  }

  protected StreamsBuilder buildTopology() {
    StreamsBuilder builder = new StreamsBuilder();
    KStream<String, FlowMsgData> inputStream = builder.stream(INPUT_TOPIC, Consumed.with(Serdes.String(), MsgDataSerdes.instance()));
    KStream<String, Annotation> outputStream = inputStream.flatMap(this::mapFlowMsgToAnnotation);
    outputStream.to(OUTPUT_TOPIC, Produced.with(Serdes.String(), AnnotationSerdes.instance()));
    return builder;
  }

}
