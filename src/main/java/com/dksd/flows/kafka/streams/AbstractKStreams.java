package com.dksd.flows.kafka.streams;

import com.dksd.flows.FlowService;
import com.dksd.flows.UserService;
import com.dksd.flows.common.client.FlowClient;
import com.dksd.flows.common.client.UserClient;
import com.dksd.flows.common.exceptions.FlowNotFoundException;
import com.dksd.flows.common.exceptions.UserNotFoundException;
import com.dksd.flows.common.helper.RuleResult;
import com.dksd.flows.common.kafka.TopicDefs;
import com.dksd.flows.common.model.*;
import com.dksd.flows.common.schema.Annotation;
import com.dksd.flows.common.schema.FlowMsgData;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler;

import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * This is my first attempt at Kafka steams to replace existing func.
 */
public abstract class AbstractKStreams {
    protected final java.util.logging.Logger logger = Logger.getLogger(getClass().getName());
    private final UserService userService;
    private final FlowService flowService;

    protected final Properties props;
    protected final FlowInfo flowInfo;
    private final StreamsUncaughtExceptionHandler exceptionHandler;

    public AbstractKStreams(UserService userService,
                            FlowService flowService,
                            Properties props,
                            FlowInfo flowInfo,
                            StreamsUncaughtExceptionHandler exceptionHandler) {
        this.userService = userService;
        this.flowService = flowService;
        this.props = props;
        this.flowInfo = flowInfo;
        this.exceptionHandler = exceptionHandler;
    }

    public KafkaStreams createKStream() {
        /* need to add these two configs props
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
         */
        props.put("application.id", getApplicationId());
        //Could also be: org.apache.kafka.streams.errors.LogAndFailExceptionHandler
        KafkaStreams ks = new KafkaStreams(buildTopology().build(), props);
        ks.setUncaughtExceptionHandler(exceptionHandler);

        //ks.store(StoreQueryParameters.fromNameAndType("name", QueryableStoreTypes.keyValueStore()));
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                ks.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        return ks;
        //kStreamsManager.runStream("anno_stream", annoStream);
        //kStreamsManager.stopStream("anno_stream");
    }

    public static Set<String> prepareFormat(RuleResult fr) {
        return fr.getUnmatchedIdsToErrors().entrySet().stream()
                .map(entry -> entry.getKey() + ":" + entry.getValue())
                .collect(Collectors.toUnmodifiableSet());
    }

    protected ProducerRecord<String, String> createDeadRecord(String clientId, String key, String value, String errMsg) {
        StringBuilder sb = new StringBuilder(clientId);
        sb.append(",");
        sb.append(value);
        sb.append(",");
        sb.append(errMsg);
        return new ProducerRecord<>(TopicDefs.DEAD_LETTERS_TOPIC, key, sb.toString());
    }

    protected UserService getUserService() {
        return userService;
    }

    protected FlowService getFlowService() {
        return flowService;
    }

    protected static List<UUID> getStoneIds(Flow flow, Set<String> destStoneNames) {
        return destStoneNames.stream()
                .map(str -> flow.getStoneByName(str).getId())
                .collect(Collectors.toUnmodifiableList());
    }

    protected void commonTracker(MsgType msgType,
                                 CheckPoint checkPoint,
                                 UUID id,
                                 int payloadSize,
                                 Flow flow,
                                 UUID srcStone,
                                 UUID destStone,
                                 RuleResult fr,
                                 UUID parentId) {
        Tracker tracker = new Tracker(id);
        tracker.setMsgType(msgType);
        tracker.setCheckPoint(checkPoint);
        tracker.setSrcStone(srcStone);
        tracker.setDestStone(destStone);
        if (fr != null) {
            tracker.setFilterRoutingRuleIds(prepareFormat(fr));
        }
        tracker.setUserId(flow.getUserId());
        tracker.setPayloadSizeBytes(payloadSize);
        tracker.setFlowId(flow.getFlowId());
        tracker.setParentId(parentId);
        flowInfo.send(id, tracker);
    }

    protected abstract String getApplicationId();

    protected abstract String getClientId();

    protected abstract StreamsBuilder buildTopology();
}
