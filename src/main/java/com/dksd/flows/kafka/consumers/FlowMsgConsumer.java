package com.dksd.flows.kafka.consumers;

import com.dksd.flows.FlowService;
import com.dksd.flows.UserService;
import com.dksd.flows.common.client.FlowClient;
import com.dksd.flows.common.client.UserClient;
import com.dksd.flows.common.exceptions.FlowNotFoundException;
import com.dksd.flows.common.exceptions.UserNotFoundException;
import com.dksd.flows.common.model.*;
import com.dksd.flows.common.schema.Annotation;
import com.dksd.flows.common.schema.FlowMsgData;
import com.dksd.flows.factory.FunctionFactory;
import com.dksd.flows.kafka.CommonFlowMsg;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler;

import java.io.IOException;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public class FlowMsgConsumer {

  private final UserService userService;
  private final FlowService flowService;
  private CommonFlowMsg commonFlowMsg;
  public final String INPUT_TOPIC;
  public final String OUTPUT_TOPIC;
  private Producer<String, String> dlProducer;
  private FunctionFactory ff = new FunctionFactory();
  private String appId;
  private final Logger logger = Logger.getLogger(getClass().getName());
  private KafkaConsumer<UUID, FlowMsgData> consumer;
  private final ExecutorService executorService = Executors.newSingleThreadExecutor();

  protected final Properties props;
  protected final FlowInfo flowInfo;
  private final KafkaProducer<UUID, Annotation> annotationProducer;

  public FlowMsgConsumer(
    UserService userService,
    FlowService flowService,
    CommonFlowMsg commonFlowMsg,
    String appId,
    String inputTopic,
    String outputTopic,
    Properties props,
    Producer<String, String> dlProducer,
    FlowInfo flowInfo,
    StreamsUncaughtExceptionHandler exceptionHandler,
    KafkaConsumer<UUID, FlowMsgData> consumer,
    KafkaProducer<UUID, Annotation> annotationProducer) {
    this.userService = userService;
    this.flowService = flowService;
    INPUT_TOPIC = inputTopic;
    OUTPUT_TOPIC = outputTopic;
    this.dlProducer = dlProducer;
    this.appId = appId;
    this.flowInfo = flowInfo;
    this.props = props;
    this.annotationProducer = annotationProducer;

    consumer.subscribe(Arrays.asList(inputTopic), new ConsumerRebalanceListener() {
      @Override
      public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
        for (TopicPartition partition : partitions) {
          logger.fine("AbstractService Partitions Revoked: " + partition.partition());
        }
      }

      @Override
      public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
        for (TopicPartition partition : partitions) {
          logger.fine("AbstractService Partitions assigned: " + partition.partition());
        }
        consumer.seekToBeginning(Collections.emptyList());
        logger.info("AbstractService: Seeked to beginning!");
      }
    });
    executorService.submit(this::consumeMessages);
  }

  public void consumeMessages() {
    while (true) {
      try {
        ConsumerRecords<UUID, FlowMsgData> consumerRecords =
          consumer.poll(Duration.of(1000, ChronoUnit.MILLIS));

        for (ConsumerRecord<UUID, FlowMsgData> record : consumerRecords) {
          User user = userService.getUser(flowInfo.getUserId(record.key()));
          Flow flow = flowService.getFlow(user, record.value());

          commonFlowMsg.identifyStoneGetAnnosFromFunction(flow,
            record.value(), flowInfo, dlProducer);
          //handle the msg here
          //cache.put(record.key(), record.value());
          //maintain order and then process in parrallel.
          //hopefully using a NIO threaded library.
          //TODO
          logger.fine("AbstractService: Received record: " + record.key() + ", " + record.value());
        }
      } catch (Exception ep) {
        logger.severe("Error reading from topic: " + INPUT_TOPIC + " ep: " + ep.getMessage());
        ep.printStackTrace();
      } finally {
        consumer.commitSync();
      }
    }
  }

}


