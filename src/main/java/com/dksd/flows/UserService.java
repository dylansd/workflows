package com.dksd.flows;

import com.dksd.flows.common.client.UserClient;
import com.dksd.flows.common.exceptions.UserNotFoundException;
import com.dksd.flows.common.model.*;

import java.io.IOException;
import java.util.Map;

public class UserService {

  protected final UserClient userClient;
  private final Map<UserId, User> userCache;

  public UserService(UserClient userClient, Map<UserId, User> userCache) {
    this.userClient = userClient;
    this.userCache = userCache;
  }

  public User getUser(UserId userId) throws UserNotFoundException {
    try {
      User cuser = userCache.get(userId);
      if (cuser != null) {
        return cuser;
      }
    } catch (NullPointerException npe) {
      //NOOP
    }
    if (userId == null) {
      throw new UserNotFoundException("User not found or id not supplied with Annotation!: " + userId);
    }
    try {
      User user = userClient.getUser(userId.getUserId()).execute().body();
      if (user == null) {
        throw new UserNotFoundException("User not found or id not supplied with Annotation!: " + userId);
      }
      userCache.put(userId, user);
      return user;
    } catch (IOException e) {
      e.printStackTrace();
      throw new UserNotFoundException("User userId not found: " + userId, e);
    }
  }

}
