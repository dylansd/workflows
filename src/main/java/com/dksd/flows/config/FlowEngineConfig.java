package com.dksd.flows.config;

import com.dksd.flows.common.client.FlowClient;
import com.dksd.flows.common.client.ServiceGenerator;
import com.dksd.flows.common.client.UserClient;
import com.dksd.flows.common.kafka.consumers.TrackerConsumer;
import com.dksd.flows.common.model.FlowInfo;
import com.dksd.flows.common.model.Tracker;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;

import java.io.IOException;
import java.util.Properties;
import java.util.UUID;

public final class FlowEngineConfig {

    public static Properties properties = new Properties();
    private FlowInfo flowInfo;

    public static void addAllProperties(Properties addProps) {
        properties.putAll(addProps);
    }

    public static String getProperty(String property) {
        return properties.getProperty(property);
    }

    public UserClient getUserClient(String address, String token) {
        return ServiceGenerator.createService(address, UserClient.class, token);
    }

    public FlowClient getFlowClient(String address, String token) {
        return ServiceGenerator.createService(address, FlowClient.class, token);
    }

    public KafkaProducer<UUID, Tracker> getTrackerKafkaProducer() {
        Properties props = new Properties();
        props.putAll(properties);
        props.put("client.id", prefix(props, "kafka.flows.flow.producer.id"));
        props.put("key.serializer", "org.apache.kafka.common.serialization.UUIDSerializer");
        props.put("value.serializer", "com.dksd.flows.common.kafka.JsonSerializer");
        return new KafkaProducer<>(props);
    }

    private String prefix(Properties props, Object property) {
        return props.get("prefix_instance_id") + "_" + props.get(property);
    }

    public synchronized FlowInfo getFlowInfo() {
        if (flowInfo == null) {
            flowInfo = new FlowInfo(getTrackerKafkaProducer());
        }
        return flowInfo;
    }

    public KafkaConsumer<UUID, Tracker> getTrackerKafkaConsumer() {
        Properties props = new Properties();
        props.putAll(properties);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, prefix(props, "kafka.flows.tracker.consumer.id"));
        props.put("key.deserializer", "org.apache.kafka.common.serialization.UUIDDeserializer");
        props.put("value.deserializer", "com.dksd.flows.common.kafka.TrackerJsonDeserializer");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        return new KafkaConsumer<>(props);
    }

    public TrackerConsumer getTrackerConsumer(FlowInfo flowInfo) throws IOException {
        return new TrackerConsumer(
                getTrackerKafkaConsumer(), flowInfo);
    }

}
